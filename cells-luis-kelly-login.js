{
  const {
    html,
  } = Polymer;
  /**
    `<cells-luis-kelly-login>` Description.

    Example:

    ```html
    <cells-luis-kelly-login></cells-luis-kelly-login>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-luis-kelly-login | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsLuisKellyLogin extends Polymer.Element {

    static get is() {
      return 'cells-luis-kelly-login';
    }

    static get properties() {
      return {
        auth:{
          type: Boolean,
          value: false,
          notify: true
        }
      };
    }

    static get template() {
      return html `
      <style include="cells-luis-kelly-login-styles cells-luis-kelly-login-shared-styles"></style>
      <slot></slot>
      
      <div class="login-form-container">
          <div class="login-form-content">
              <form name="login" method="POST" on-submit="_handleSubmit" class="form">
                  <h1>login-component-form</h1>
                  <div class="row">
                      <input type="text" name="username" placeholder="Username">
                  </div>
                  <div class="row">
                      <input type="password" name="password" placeholder="Password">
                  </div>
                  <input type="submit" value="Login">
              </form>
          </div>
      </div>
      
      `;
    }

    _handleSubmit(e){
      e.preventDefault();
      let uz = e.target.username.value,
          px = e.target.password.value,
          p = "bHVpczpwb2x5bWVyMg==";
      let enc = btoa(`${uz}:${px}`);
      if(enc===p){
          this.set("auth",true);
          this.dispatchEvent(new CustomEvent("auth-success",{detail:{"key":123}}));
      }else{
          this.set("auth",false)
          this.dispatchEvent(new CustomEvent("auth-error",{detail:{"key":123}}));
      }
    }

  }

  customElements.define(CellsLuisKellyLogin.is, CellsLuisKellyLogin);
}